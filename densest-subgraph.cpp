#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <dirent.h>
#include <list>
#include <queue>

using namespace std;
const string output_folder = "output";

#if !defined GOLDBERG && !defined CHARIKAR
#define CHARIKAR
#endif

#ifdef GOLDBERG
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/one_bit_color_map.hpp>
#include <boost/graph/boykov_kolmogorov_max_flow.hpp>
#include <boost/property_map/property_map.hpp>
#include <boost/typeof/typeof.hpp>

typedef pair<int,int> edge;

typedef boost::adjacency_list_traits < boost::vecS, boost::vecS, boost::directedS > Traits;
typedef boost::adjacency_list < boost::vecS, boost::vecS, boost::directedS,
    boost::property < boost::vertex_name_t, std::string,
    boost::property < boost::vertex_index_t, long,
    boost::property < boost::vertex_color_t, boost::default_color_type,
    boost::property < boost::vertex_distance_t, long,
    boost::property < boost::vertex_predecessor_t, Traits::edge_descriptor > > > > >,

    boost::property < boost::edge_capacity_t, double,
    boost::property < boost::edge_residual_capacity_t, double,
    boost::property < boost::edge_reverse_t, Traits::edge_descriptor > > > > graph;

//--------------------------------------------------------------------
/// Add new edge to boost::graph
//--------------------------------------------------------------------
void addEdge(int v1, int v2, boost::property_map < graph, boost::edge_reverse_t >::type &rev, 
			 const double capacity, graph &g)
{
  boost::add_edge(v1, v2, capacity, g);
  boost::add_edge(v2, v1, capacity, g);
  graph::edge_descriptor e1 = boost::edge(v1, v2, g).first;
  graph::edge_descriptor e2 = boost::edge(v2, v1, g).first;
  rev[e1] = e2;
  rev[e2] = e1;
}

//--------------------------------------------------------------------
/// Finding the maximum dense subgraph of a graph
/// @param n: the number of graph vertice
/// @param edges: the list of graph edges
/// @param degrees: the degree of graph vertice
/// @return density: the density of the densest subgraph |E|/|V|
/// @reference: http://digitalassets.lib.berkeley.edu/techreports/ucb/text/CSD-84-171.pdf
//--------------------------------------------------------------------
double find_maximum_dense_subgraph(int n, list<edge> edges, int *degrees) {
  graph g(n+2);
  boost::property_map < graph, boost::edge_capacity_t >::type capacity = get(boost::edge_capacity, g);
  boost::property_map < graph, boost::edge_reverse_t >::type rev = get(boost::edge_reverse, g);
  boost::property_map < graph, boost::vertex_color_t >::type colors = get(boost::vertex_color, g);

  int m = edges.size();
  for (list<edge>::const_iterator it = edges.begin(); it != edges.end(); it++) {
    addEdge((*it).first, (*it).second, rev, 1.0, g);
  }

  for (int i = 0; i < n; i++) {
	addEdge(n, i, rev, m, g);
	addEdge(n+1, i, rev, 0, g);
  }

  double l = 0, u = m, guess, density;
  //double delta = 1.0/(n*(n-1));
  while ((u-l)*n*(n-1) >= 1.0) {
	guess = (u+l)/2;

	// construct network
	for (int i = 0; i < n; i++) {
	  graph::edge_descriptor ed = boost::edge(i, n+1, g).first;
	  boost::put(boost::edge_capacity_t(), g, ed, m+2*guess-degrees[i]);
	  ed = boost::edge(n+1, i, g).first;
	  boost::put(boost::edge_capacity_t(), g, ed, m+2*guess-degrees[i]);
	}

	// run the Boykov Kolmogorov algorithm to find out max-flow min-cut of the graph
	boykov_kolmogorov_max_flow(g, n, n+1);

	size_t i;
	for (i = 0; i < num_vertices(g); ++i) {
	  if (colors[i] == colors[n])
		break;
	}
	if (i == n)
	  u = guess;
	else {
	  l = guess;
	  density = guess;

#ifdef DEBUG
	  int n_edges = 0, n_vertice = 0;
	  for (list<edge>::const_iterator it = edges.begin(); it != edges.end(); it++) {
		if (colors[ (*it).first ] == colors[n] && colors[ (*it).second ] == colors[n])
		  n_edges++;
	  }
	  
	  for (int i = 0; i < num_vertices(g); ++i) {
		cout << colors[i] << " ";
		if (colors[i] == colors[n])
		  n_vertice++;
	  }
	  cout << "\nGuess: " << guess << endl;
	  cout << "edges: " << n_edges << endl;
	  cout << "vertice: " << n_vertice-1 << endl;
	  cout << "edges/vertice: " << n_edges*1.0/(n_vertice-1) << endl;
#endif
	}
  }

  return density;
}
#endif

#ifdef CHARIKAR
//--------------------------------------------------------------------
/// Finding the maximum dense subgraph of a graph
/// @param n, m: the number of graph vertice, edges
/// @param adjacencyList: the adjacency list
/// @param degrees: the degree of graph vertice
/// @return density: the density of the densest subgraph |E|/|V|
/// @reference: https://github.com/tsourolampis/Densest-subgraph-peeling-algorithm
//--------------------------------------------------------------------
double charikarPeel(int n, int m, list<int>* adjacencyList, int *degrees) {
  priority_queue< pair<int, int>, vector<pair<int, int> >, greater<pair<int, int> > > q;
  double EDGEDENSITY = 2.0*m/n;
  double CharikarFe = (double) 2*m/(n*(n-1)); 
  int CharikarSize = n; 
  double CharikarSizeFraction = 1.0;
  double numedges = (double)m;
  double numvertices=(double)n;

  int temp_degrees[n];
  for (int i = 0; i < n; ++i) {
	q.push(make_pair(degrees[i], i));
	temp_degrees[i] = degrees[i];
  }

  int counter = 0; 
  while (!q.empty()) {
	int c = q.top().second;
	q.pop();
	if (temp_degrees[c] < 0 ) { 
	  continue;
	}

	numedges -= temp_degrees[c];  
	--numvertices;
	temp_degrees[c] = -1;  
	if (numvertices > 0) {
	  double cur = 2.0*numedges / (double) numvertices;  
	  if (cur >= EDGEDENSITY ) {
		EDGEDENSITY = cur;
		CharikarFe = EDGEDENSITY/(numvertices-1);
		CharikarSize = numvertices;
		CharikarSizeFraction = numvertices/n;
	  }
	}

	list<int>::iterator it;
	for (it = adjacencyList[c].begin(); it != adjacencyList[c].end(); it++) {
	  if(degrees[*it]>0) {
		q.push(make_pair(--temp_degrees[*it], *it));
	  }
	}
	if (counter == n)
	  break;
  }
  return EDGEDENSITY/2;
}
#endif

//--------------------------------------------------------------------
/// Generate Barabasi-Albert model graph
/// and calculate the density of that graph
/// @param n, d: parameters of Random Graph
//--------------------------------------------------------------------
#if defined GOLDBERG && defined CHARIKAR
void barabasi_albert_model(int n, int d, fstream &fs1, fstream &fs2) {
#else
void barabasi_albert_model(int n, int d, fstream &fs) {
#endif

#ifdef GOLDBERG
  list<edge> edges;
#endif
#ifdef CHARIKAR
  list<int> *adj = new list<int>[n];
#endif

  int *degrees = new int[n]; // store degree of each vertex
  bool *is_selected = new bool[n];
  for (int i = 0; i < n; i++)
	is_selected[i] = false;

  int *selected_vertice = new int[d];

  for (int i = 0; i < d; i++) {
	degrees[i] = 1;
#ifdef GOLDBERG
	edges.push_back(edge(d, i));
#endif
#ifdef CHARIKAR
	adj[d].push_back(i);
	adj[i].push_back(d);
#endif
  }
  degrees[d] = d;

  int num_links = d;
  for (int i = d+1; i < n; i++) {
	int range = 2*num_links;

	// link new node to d old nodes
	for (int j = 0; j < d; j++) {
	  int random = rand() % range;
#if defined DEBUG
	  cout << "Random = " << random << endl;
#endif
	  int selected_vertex;
	  int cumulative = 0;
	  for (selected_vertex = 0; selected_vertex < i; selected_vertex++) {
		if (is_selected[ selected_vertex ])
		  continue;
		cumulative += degrees[ selected_vertex ];
		if (random < cumulative)
		  break;
	  }
#if defined DEBUG
	  cout << "Selected_Vertex = " << selected_vertex << endl;
#endif
	  
	  selected_vertice[j] = selected_vertex;
	  is_selected[ selected_vertex ] = true;
	  
	  range -= degrees[ selected_vertex ];
	}
	
	// update degree of selected vertice
	for (int j = 0; j < d; j++) {
	  degrees[ selected_vertice[j] ]++;
	  is_selected[ selected_vertice[j] ] = false;
#ifdef GOLDBERG
	  edges.push_back(edge(i, selected_vertice[j]));
#endif
#ifdef CHARIKAR
	  adj[ selected_vertice[j] ].push_back(i);
	  adj[i].push_back(selected_vertice[j]);
#endif
	}
	degrees[i] = d;

	num_links += d;

	if ((i+1) % n == 0) {
#if defined GOLDBERG && defined CHARIKAR
	  fs1 << find_maximum_dense_subgraph(i+1, edges, degrees) << " ";
	  fs2 << charikarPeel(i+1, d*(i+1-d), adj, degrees) << " ";
#else
#ifdef GOLDBERG
	  fs << find_maximum_dense_subgraph(i+1, edges, degrees) << " ";
#endif
#ifdef CHARIKAR
	  fs << charikarPeel(i+1, d*(i+1-d), adj, degrees) << " ";
#endif
#endif
	}
  }

#if defined GOLDBERG && defined CHARIKAR
  fs1 << endl;
  fs2 << endl;
#else
  fs << endl;
#endif

  delete [] degrees;
  delete [] selected_vertice;
  delete [] is_selected;
#ifdef CHARIKAR
  delete [] adj;
#endif
}

int getOutputFileNumber(int n, int d) {
  DIR *dir;
  dirent *pdir;
  int t_max = 0;

  dir = opendir(("./" + output_folder).c_str());
  int n_temp, d_temp, t_temp;
  string filename_temp;
  while (pdir = readdir(dir)) {
	filename_temp = pdir->d_name;
	n_temp = d_temp = t_temp = 0;
#ifdef GOLDBERG
	sscanf(filename_temp.c_str(), "n%d_d%d_goldberg_%d", &n_temp, &d_temp, &t_temp);
#endif
#ifdef CHARIKAR
	sscanf(filename_temp.c_str(), "n%d_d%d_charikar_%d", &n_temp, &d_temp, &t_temp);
#endif
	if (n == n_temp && d == d_temp)
	  if (t_temp > t_max) {
		t_max = t_temp;
	  }
  }
  return t_max+1;
}

//--------------------------------------------------------------------
/// Process parameters
/// @param argc, argv: input parameters
/// @param n, d: parameters of Random Graph
/// @param t: the number of tries
/// @return true if successful
///         false otherwise
//--------------------------------------------------------------------
bool processInput(int argc, char *argv[], int &n, int &d, int &t) {
  if (argc >= 3) {
    istringstream iss1( argv[1] );
    if (!(iss1 >> n))
      return false;
    istringstream iss2( argv[2] );
    if (!(iss2 >> d))
      return false;
  }
  else {
	return false;
  }
  if (argc >= 4) {
	istringstream iss3( argv[3] );
    if (!(iss3 >> t))
	  return false;
  }
  return true;
}

int main(int argc, char *argv[]) {
  srand(unsigned(time(0)));

  int n, d, tries = 1000;
  // process input
  if (processInput(argc, argv, n, d, tries) == false)
	return 0;
  
  int fileNumber = getOutputFileNumber(n, d);
#ifdef GOLDBERG
  fstream fs1;
  char filename1[128];
  snprintf(filename1, sizeof(filename1), "n%d_d%d_goldberg_%d.txt", n, d, fileNumber);
  fs1.open((output_folder + "/" + filename1).c_str(), fstream::out | fstream::app);
#endif
#ifdef CHARIKAR
  fstream fs2;
  char filename2[128];
  snprintf(filename2, sizeof(filename2), "n%d_d%d_charikar_%d.txt", n, d, fileNumber);
  fs2.open((output_folder + "/" + filename2).c_str(), fstream::out | fstream::app);
#endif
  
  for (int i = 0; i < tries; i++) {
#if defined GOLDBERG && defined CHARIKAR
	barabasi_albert_model(n, d, fs1, fs2);
#else
#ifdef GOLDBERG
	barabasi_albert_model(n, d, fs1);
#endif
#ifdef CHARIKAR
	barabasi_albert_model(n, d, fs2);
#endif
#endif
  }

#ifdef GOLDBERG
  fs1.close();
#endif
#ifdef CHARIKAR
  fs2.close();
#endif
  return 0;
}
